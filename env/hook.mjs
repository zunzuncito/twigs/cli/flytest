

export default class Hook {
  constructor(target, class_name, socket) {
    this.target = target;
    this.class_name = class_name;
    this.socket = socket;

    this.incoming_queue = [];

  }

  async cmd(cmd_str, method, args) {
    console.debug(`> Hook ${this.target}@${this.class_name} request > Method: ${method}; Arguments: ${JSON.stringify(args)}`);
    this.socket.write(JSON.stringify({
      target: this.target,
      class: this.class_name,
      cmd: cmd_str,
      method: method,
      args: args
    })+"\n");

    const _this = this;
    return await new Promise((resolve) => {
      const check_response = () => {
        for (let i = _this.incoming_queue.length-1; i >= 0; i--) {
          if (
            (
              cmd_str == "start" &&
              _this.incoming_queue[i] &&
              _this.incoming_queue[i].info == 'hook-started'
            ) || (
              cmd_str == "call" &&
              _this.incoming_queue[i] &&
              _this.incoming_queue[i].info == 'method-called'
            )
          ) {
            const returned_value = _this.incoming_queue[i].return;
            _this.incoming_queue.splice(i, 1);
            resolve(returned_value)
            return;
          }
        }
        setTimeout(check_response, 333);
      }
      check_response();
    });
  }

  async start() {
    await this.cmd("start");
  }

  incoming(data) {
    console.debug(`< Hook ${this.target}@${this.class_name} response < Message: ${JSON.stringify(data)}`);
    this.incoming_queue.push(data);
  }
 

}
