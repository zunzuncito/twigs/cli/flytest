
import fs from "fs"
import path from "path"

import { fileURLToPath } from 'url';
const __dirname = path.dirname(fileURLToPath(import.meta.url));

import YAML from "yaml"

import Container from "./iso/contain.mjs"
import VM from "./iso/vm.mjs"


import FlyModule from "zunzun/flymodule/index.mjs"
import FlyCfg from 'zunzun/flycfg/index.mjs'

import FSUtil from 'zunzun/flyutil/fs.mjs'
import ObjectUtil from 'zunzun/flyutil/object.mjs'
import ExecUtil from 'zunzun/flyutil/exec.mjs'

import TimeUtil from 'zunzun/flyutil/time.mjs'
import LogUtil from 'zunzun/flyutil/log.mjs'

import TwigsUtil from 'zunzun/flyutil/twigs.mjs'


import TCPServer from './tcp-server.mjs'
import Hook from './hook.mjs'

const CONFIG_FILES = [
  "twig.json",
  ".zunzun.yaml",
  "package.json"
];


const TIME_FORMAT = ['hh', ':', 'mm', ':', 'ss', ':', 'ms'];

class CfgObject {
  static force_add_files(file_a, file_b) {
    let dst_obj = {};
    if (fs.existsSync(file_a)) {
      try {
        const dst_content = fs.readFileSync(file_a, "utf8");
        dst_obj = YAML.parse(dst_content);
      } catch (e) {
        console.log(`File: ${file_a}`);
        console.error(e.stack);
      }
    }

    let src_obj = {};
    try {
      const src_content = fs.readFileSync(file_b, "utf8");
      src_obj = YAML.parse(src_content);
    } catch (e) {
      console.log(`File: ${file_b}`);
      console.error(e.stack);
    }

    for (const key in src_obj) {
      if (key == "services") {
        if (!dst_obj[key]) dst_obj[key] = [];
        for (const src_srv_cfg of src_obj[key]) {
          let exists = false;
          for (let d = 0; d < dst_obj[key].length; d++) {
            if (src_srv_cfg.name == dst_obj[key][d].name) {
              exists = true;
              dst_obj[key][d] = ObjectUtil.add(dst_obj[key][d], src_srv_cfg);
            }
          }

          if (!exists) {
            dst_obj[key].push(src_srv_cfg);
          }
        }
      } else {
        dst_obj[key] = dst_obj[key] ? ObjectUtil.add(dst_obj[key], src_obj[key]) : src_obj[key];
      }

    }

    let out_string = YAML.stringify(dst_obj);

    fs.writeFileSync(file_a, out_string);
  } 
}

class FlyObject {
  static force_add_files(file_a, file_b, file_type) {
    let dst_obj = undefined;
    if (fs.existsSync(file_a)) {
      try {
        const dst_content = fs.readFileSync(file_a, "utf8");
        switch (file_type) {
          case "yaml":
            dst_obj = YAML.parse(dst_content);
            break;
          case "json":
            dst_obj = JSON.parse(dst_content);
            break;
          default:
            dst_obj = JSON.parse(dst_content);

        }
      } catch (e) {
        console.log(`File: ${file_a}`);
        console.error(e.stack);
        dst_obj = {};
      }
    }


    let src_obj = undefined;
    try {
      const src_content = fs.readFileSync(file_b, "utf8");
      switch (file_type) {
        case 'yaml':
          src_obj = YAML.parse(src_content);
          break;
        case 'json':
          src_obj = JSON.parse(src_content);
          break;
        default:
          src_obj = JSON.parse(src_content);

      }
    } catch (e) {
      console.log(`File: ${file_b}`);
      console.error(e.stack);
      src_obj = {};
    }


    const result = dst_obj ? ObjectUtil.add(dst_obj, src_obj) : src_obj;

    let out_string = undefined;
    switch (file_type) {
      case 'yaml':
        out_string = YAML.stringify(result);
        break;
      case 'json':
        out_string = JSON.stringify(result);
        break;
      default:
        out_string = JSON.stringify(result);
    }
    fs.writeFileSync(file_a, out_string);
  }
}

export default class TestingEnvironment {
  static async construct(project_path, cfg, zuncfg, basecfg) {
    const _this = new TestingEnvironment(project_path, cfg, zuncfg, basecfg);

    if (!fs.existsSync(_this.env_path)) fs.mkdirSync(_this.env_path);

    if (!cfg.no_srv) {
      const env_config_path = path.resolve(_this.env_path, '.zunzun.yaml');
      if (!fs.existsSync(env_config_path)) fs.writeFileSync(env_config_path, 'name: test_app');
    }

    const env_tcfg_path = path.resolve(_this.env_path, 'twig.json');
    if (!fs.existsSync(env_tcfg_path)) fs.writeFileSync(env_tcfg_path, '{}');

    const env_npm_pkg_path = path.resolve(_this.env_path, 'package.json');
    if (!fs.existsSync(env_npm_pkg_path)) fs.writeFileSync(env_npm_pkg_path, '{}');

    const env_twigs_path = path.resolve(_this.env_path, 'twigs');
    if (!fs.existsSync(env_twigs_path)) fs.mkdirSync(env_twigs_path);

    const env_srv_path = path.resolve(env_twigs_path, 'srv');
    if (!fs.existsSync(env_srv_path)) fs.mkdirSync(env_srv_path);

    _this.env_cfg = (await FlyCfg.project(_this.env_path)).app_cfg;

    for (let m = 0; m < _this.env_cfg.services.length; m++) {
      const srv_name = _this.env_cfg.services[m].name;
      for (let env_srv of _this.test_cfg.services) {
        if (srv_name == env_srv.name) {
          _this.env_cfg.services[m].config = ObjectUtil.force_add(_this.env_cfg.services[m], env_srv.config);
        }
      }
    }

    for (let srv of _this.env_cfg.services) {
      for (let env_srv of _this.test_cfg.services) {
        if (srv.name == env_srv.name) {
//          console.log(srv.name, srv.config);
        }
      }
    }

    return _this;
  }

  constructor(project_path, cfg, zuncfg, basecfg) {
    this.select_twigs = cfg.select_twigs;
    this.select_cases = cfg.select_cases;


    this.appcfg = basecfg;
    this.cfg = cfg;
    this.zuncfg = zuncfg;

    this.project_path = project_path;
    this.flytest_path = path.resolve(project_path, cfg.file_names.tests);
    this.env_path = path.resolve(this.flytest_path, "env");
    this.log_path = path.resolve(this.env_path, cfg.file_names.srv_log);
    this.test_cfg_path = path.resolve(this.flytest_path, cfg.file_names.cfg);
    this.test_cfg = YAML.parse(fs.readFileSync(this.test_cfg_path, "utf8"));
    this.test_project_path = this.test_cfg.container ? this.test_cfg.project_dir : this.env_path;
    this.project_twig_path = path.resolve(project_path, "twig.json");
    this.env_twig_path = path.resolve(this.env_path, "twig.json");

    this.secondary_project_paths = {};
    for (let sec_project in this.test_cfg.secondary_project_dirs) {
      this.secondary_project_paths[sec_project] = path.resolve(this.test_cfg.secondary_project_dirs[sec_project]);
    }

    if (this.test_cfg.container && this.test_cfg.vm) {
      throw new Error(`Cannot use both container and VM at the same time!`);
    } else if (this.test_cfg.container) {
      this.container = new Container(this.project_path, this.test_cfg.container);
      if (cfg.reset_volumes) this.container.reset_volumes();
      this.zuncfg.container = this.container;
    } else if (this.test_cfg.vm) {
      this.container = new VM(this.project_path, this.test_cfg.container);
    }

    this.tools = {
      mocks: {},
      fixtures: {},
      hooks: {}
    };

    this.hooks = [];
  }

  get_tests(twig_type) {
    const available_tests = [];



    let project_twig_json = undefined;
    try {
      project_twig_json = JSON.parse(fs.readFileSync(this.project_twig_path, 'utf8'));
    } catch (e) {
      console.error(e.stack);
    }


    const layers = TwigsUtil.get_layers(project_twig_json, this.zuncfg);

    const twigs_dir = path.resolve(this.env_path, "twigs");

    for (let i = layers.length-1; i >= 0; i--) {
      const layer = layers[i];
      for (let twig of layer) {
        const twigs_type_dir = path.resolve(twigs_dir, twig.type);
        const tests_dir = path.resolve(twigs_type_dir, twig.name, this.cfg.file_names.tests);
        if (twig.type == twig_type && fs.existsSync(tests_dir)) {
          available_tests.push({
            type: twig.type,
            name: twig.name,
            path: tests_dir
          });
        }
      }
    }

    return available_tests;
  }

  async setup_twig_tool(type, tools_path, tool_cfg, tool_name, socket) {

    const _this = this;
    if (type == 'hooks') {

      this.tools[type][tool_name] = async (class_name) => {
        console.log("SETUP HOOK", tool_name, class_name);

        const remote_hook_name = tool_name.includes(":") ? tool_name.split(":")[1] : tool_name;
        const new_hook = new Hook(remote_hook_name, class_name, socket);
        _this.hooks.push(new_hook);
        await new_hook.start();
        return new_hook;
      };
    } else {
      const test_module = await FlyModule.load(tools_path, "index.mjs");

      for (let prop in test_module.default) {
        if (!this.tools[type][tool_name]) {
          this.tools[type][tool_name] = {};
        }
        this.tools[type][tool_name][prop] = async () => {

          if (
            typeof test_module.default[prop].construct == "function" &&
            test_module.default[prop].construct.constructor.name == 'AsyncFunction'
          ) {
            return await test_module.default[prop].construct({
              twig: tool_cfg,
              env: _this.env_cfg,
              zun: _this.zuncfg
            }, _this.tools, socket);
          }

        };
      }
    }

  }

  async setup_tools(type, socket, project_path, tool_name_prefix) {
    try {
      let project_twig_json = undefined;
      try {
        const project_twig_path = path.resolve(project_path, 'twig.json');
        project_twig_json = JSON.parse(fs.readFileSync(project_twig_path, 'utf8'));
      } catch (e) {
        console.error(e.stack);
      }

      if (!tool_name_prefix) tool_name_prefix = '';
 
      const app_tools_path = path.join(this.flytest_path, type);
      const app_tools_index = path.join(app_tools_path, "index.mjs");

      if (fs.existsSync(app_tools_index)) {
        await this.setup_twig_tool(type, app_tools_path, { cwd: this[`env_path`] }, tool_name_prefix+`subject`, socket);
      }

      const layers = TwigsUtil.get_layers(project_twig_json, this.zuncfg);
      const twigs_dir = path.resolve(project_path, "twigs");
      for (let i = layers.length-1; i >= 0; i--) {
        const layer = layers[i];
        for (let twig of layer) {
          const twigs_type_dir = path.join(twigs_dir, twig.type);
          const tests_dir = path.join(twigs_type_dir, twig.name, this.cfg.file_names.tests);
          const tools_path = path.join(tests_dir, type);
          const tools_index_path = path.join(tools_path, "index.mjs");

          if (fs.existsSync(tools_index_path)) {
            let tool_cfg = undefined;
            for (let srv_cfg of this.env_cfg.services) {
              if (srv_cfg.name == twig.name) {
                tool_cfg = srv_cfg.config;
                break;
              }
            }
            if (!tool_cfg) tool_cfg = {};
            tool_cfg.cwd = this[`env_path`];

            await this.setup_twig_tool(type, tools_path, tool_cfg, tool_name_prefix+`${twig.type}/${twig.name}`, socket);
          }
        }
      }
      return this.tools[type];
    } catch (e) {
      console.error(e.stack);
    }
  }

  sync() {
    const twig_types = ["lib", "srv"];

    let project_twig_json = undefined;
    try {
      project_twig_json = JSON.parse(fs.readFileSync(this.project_twig_path, 'utf8'));
    } catch (e) {
      console.error(e.stack);
      project_twig_json = {};
    }



    /*
     * TODO
     * - sync configuration files
     * - sync other files
     * - install npm and twigs deps
     */


    console.log("SYNC", this.project_twig_path);
    ExecUtil.command(`cp -f ${this.project_twig_path} ${this.env_twig_path}`, null, true);

    const proj_config_path = path.join(this.project_path, ".zunzun.yaml");
    const env_config_path = path.join(this.env_path, ".zunzun.yaml");
    ExecUtil.command(`cp -f ${proj_config_path} ${env_config_path}`, null, true);

    if (this.test_cfg.services) {
      const env_config = YAML.parse(fs.readFileSync(env_config_path, 'utf8'));
      for (let srv of this.test_cfg.services) {
        let existing = false;
        for (let i = 0; i < env_config.services.length; i++) {
          if (env_config.services[i].name == srv.name) {
            console.log("existing" , env_config.services[i], srv);
            existing = true;
            env_config.services[i] = ObjectUtil.force_add(env_config.services[i], srv);
            break;
          }
        }
        if (!existing) {
          console.log("ADD NEW", srv);
          env_config.services.push(srv);
        }
      }
      fs.writeFileSync(env_config_path, YAML.stringify(env_config));
    }

    for (let twig_type of twig_types) {
      const twig_tests = this.get_tests(twig_type);
      for (let twig_test of twig_tests) {
        const tests_env_dir = path.resolve(twig_test.path, "env");
        if (fs.existsSync(tests_env_dir)) {
          const test_env_files = fs.readdirSync(tests_env_dir);

          for (let test_env_file of test_env_files) {
            const test_file_path = path.resolve(tests_env_dir, test_env_file);
            const dst_path = path.resolve(this.env_path, test_env_file);

            if (test_env_file == ".zunzun.yaml") {
              CfgObject.force_add_files(dst_path, test_file_path);
            } else if (test_env_file.endsWith(".yaml") || test_env_file.endsWith(".yml")) {
              FlyObject.force_add_files(dst_path, test_file_path, 'yaml');
            } else if (test_env_file.endsWith(".json")) {
              FlyObject.force_add_files(dst_path, test_file_path);
            } else if (fs.lstatSync(test_file_path).isDirectory()) {
              FSUtil.sync_dirs(test_file_path, dst_path);
            }

          }

        }
      }
    }

    if (this.test_cfg.sync && this.test_cfg.sync.dirs) {
      for (let dir of this.test_cfg.sync.dirs) {
        const src_dir = path.join(this.project_path, dir);
        const dst_dir = path.join(this.env_path, dir);
        ExecUtil.command(`cp -rf ${src_dir} ${dst_dir}`, null, true);
      }
    }


    if (!this.cfg.no_npm) ExecUtil.command("npm install", this.env_path, true);

    if (!this.cfg.no_twigs) {
      ExecUtil.command("mkdir -p twigs/srv", this.env_path, true);
      ExecUtil.command("mkdir -p twigs/lib", this.env_path, true);
      ExecUtil.command("mkdir -p node_modules/zunzun", this.env_path, true);
      ExecUtil.command("twigs install --sync-local", this.env_path, true);
    }

    ExecUtil.command("zuncli flypg init --testing", this.env_path, true);
  }




  count_tests_old(module_dir, cfg, statistics) { // TODO remove
    const tests_dir = path.resolve(module_dir, cfg.file_names.tests);
    const tests_cfg_path = path.join(tests_dir, cfg.file_names.suite, cfg.file_names.cfg);

    if (fs.existsSync(tests_cfg_path)) {
      const tests_cfg = YAML.parse(fs.readFileSync(tests_cfg_path, "utf8"));

      if (tests_cfg && tests_cfg.cases) {
        for (let test_target of tests_cfg.cases) {

          if (!this.select_cases || this.select_cases.includes(test_target.file)) {
            if (test_target.methods) {
              for (let test_method of test_target.methods) {
                statistics.total++;
              }
            }
          }
        }
      }
    }
  }

  count_tests(suite_path, cfg, statistics) {
    const tests_cfg_path = path.join(suite_path, cfg.file_names.cases);
    if (fs.existsSync(tests_cfg_path)) {
      const test_cases = YAML.parse(fs.readFileSync(tests_cfg_path, "utf8"));
      if (test_cases) {
        for (let test_target of test_cases) {
          if (!this.select_cases || this.select_cases.includes(test_target.file)) {
            if (test_target.methods) {
              for (let test_method of test_target.methods) {
                statistics.total++;
              }
            }
          }
        }
      }
    }
  }


  async test() {
    try {
      const _this = this;

      const test_time = Date.now();

      const test_path = this.env_path;

      const zunzun_config = this.env_cfg;

      console.log("this.appcfg", this.appcfg);
      if (!this.test_cfg.no_srv && this.appcfg) {
        for (let srv of this.appcfg.services) {
          for (let env_srv of this.env_cfg.services) {
            if (srv.name == env_srv.name) {
              srv.config = ObjectUtil.force_add(srv.config, env_srv.config);
            }
          }
        }
      }


      if (!this.cfg.env_only) {
        _this.tcp_server = await TCPServer.construct({
          host: "0.0.0.0",
          port: 39637
        }, _this);
      }

      const test_cfg = this.env_cfg;


      if (!this.cfg.no_env) {
        if (this.container) {
          this.container.spawn();
        } else {
          const log_path = this[`log_path`];
          console.log("Starting test application...");
          const test_app_cmd = this.test_cfg.test_application || "zunsrv";
          const test_app_process = this.test_app_process = ExecUtil.spawn(test_app_cmd, test_path);
          console.log(`Process spawned PID: ${test_app_process.pid}`);

          const ostream = this.test_app_ostream = fs.createWriteStream(log_path);
          test_app_process.stdout.pipe(ostream);
          test_app_process.stderr.pipe(ostream);
        }
      }

      if (this.cfg.env_only) {
        await new Promise((resolve) => {
          console.log("Keeping environment running!");
        });
      } else if (this.container) {
        console.log(" > > Spawn subject!");
        ExecUtil.command(`${this.container.tool} exec -i ${this.container.name} bash -c "${this.container.subject_cmd}"`);
      }


      console.log("TCP");
      _this.test_app_socket = await _this.tcp_server.socket();
      _this.secondary_sockets = {};


      console.log("TCP DONE");

      if (this.container && Array.isArray(this.container.secondary_subjects)) {
        _this.secondary_sockets = {};
        for (let scmd of this.container.secondary_subjects) {
          ExecUtil.command(`${this.container.tool} exec -i ${this.container.name} bash -c "${scmd}"`);
          const nss = await _this.tcp_server.next_subject_socket();
          if (nss) {
            _this.secondary_sockets[nss.subject_name] = nss.socket;
          } else {
            throw new Error("Invalid next subject socket!");
          }
        }
      }

      const cfg = this.cfg;


      const statistics = {
        total: 0,
        cti: 1,
        passed: 0,
        failed: 0
      }

//      console.log("this.select_twigs", this.select_twigs);

      LogUtil.nl();
      LogUtil.msg([1, 46, 37, " > > COUNTING TESTS > > ", 0]);
      LogUtil.nl();


      const subject_suite_dir = path.join(
        this.project_path,
        cfg.file_names.tests,
        cfg.file_names.suite
      );
      this.count_tests(subject_suite_dir, cfg, statistics);

      const twigs_dir = path.join(test_path, 'twigs');
      const twigs_lib_dir = path.join(twigs_dir, 'lib');
      if (fs.existsSync(twigs_lib_dir)) {
        const twig_libs = fs.readdirSync(twigs_lib_dir).filter((value) => {
          return !this.select_twigs || 
            (this.select_twigs && this.select_twigs.includes(`lib/${value}`));
        });
        for (let twig_lib of twig_libs) {
          const module_dir = path.join(twigs_lib_dir, twig_lib);
          const suite_dir = path.join(
            module_dir,
            cfg.file_names.tests,
            cfg.file_names.suite
          );
          this.count_tests(suite_dir, cfg, statistics);
        }
      }

      const twigs_srv_dir = path.join(twigs_dir, 'srv');
      if (fs.existsSync(twigs_srv_dir)) {
        const twig_srvs = fs.readdirSync(twigs_srv_dir).filter((value) => {
          return !this.select_twigs || 
            (this.select_twigs && this.select_twigs.includes(`srv/${value}`));
        });
        for (let twig_srv of twig_srvs) {
          const module_dir = path.join(twigs_srv_dir, twig_srv);
          const suite_dir = path.join(
            module_dir,
            cfg.file_names.tests,
            cfg.file_names.suite
          );
          this.count_tests(suite_dir, cfg, statistics);
        }
      }

      LogUtil.nl();
      LogUtil.msg([1, 46, 37, " > > SETTING UP TOOLS > > ", 0]);
      LogUtil.nl();

      await this.setup_tools("mocks", this.test_app_socket, this.test_project_path);
      await this.setup_tools("fixtures", this.test_app_socket, this.test_project_path);
      await this.setup_tools("hooks", this.test_app_socket, this.test_project_path);
      for (let sec_project in this.secondary_project_paths) {
        await this.setup_tools(
          "mocks",
          this.secondary_sockets[sec_project],
          this.secondary_project_paths[sec_project],
          `${sec_project}:`
        );
        await this.setup_tools(
          "fixtures",
          this.secondary_sockets[sec_project],
          this.secondary_project_paths[sec_project],
          `${sec_project}:`
        );
        await this.setup_tools(
          "hooks",
          this.secondary_sockets[sec_project],
          this.secondary_project_paths[sec_project],
          `${sec_project}:`
        );

      }
//      await this.setup_fixtures();

      LogUtil.nl();
      LogUtil.msg([1, 46, 37, " > > TESTING SUBJECT > > ", 0]);
      LogUtil.nl();

      await this.run_test_suite(subject_suite_dir, null, zunzun_config, statistics);

      LogUtil.nl();
      LogUtil.msg([1, 46, 37, " > > TESTING LIBRARIES > > ", 0]);
      LogUtil.nl();


      if (fs.existsSync(twigs_lib_dir)) {
        const twig_libs = fs.readdirSync(twigs_lib_dir).filter((value) => {
          return !this.select_twigs || 
            (this.select_twigs && this.select_twigs.includes(`lib/${value}`));
        });
        for (let twig_lib of twig_libs) {
          const module_dir = path.resolve(twigs_lib_dir, twig_lib);
          LogUtil.msg([1, 44, 37, " > LIBRARY >", 0], " > ", twig_lib);
          const tests_dir = path.join(module_dir, cfg.file_names.tests);
          const suite_dir = path.join(tests_dir, cfg.file_names.suite);
          await this.run_test_suite(suite_dir, null, zunzun_config, statistics);
        }
      }

      LogUtil.nl();
      LogUtil.msg([1, 46, 37, " > > TESTING SERVICES > > ", 0]);
      LogUtil.nl();


      if (fs.existsSync(twigs_srv_dir)) {
        const twig_srvs = fs.readdirSync(twigs_srv_dir).filter((value) => {
          return !this.select_twigs || 
            (this.select_twigs && this.select_twigs.includes(`srv/${value}`));
        });
        for (let twig_srv of twig_srvs) {
          const module_dir = path.resolve(twigs_srv_dir, twig_srv);

          let selected_cfg = undefined;
          for (let srv_cfg of zunzun_config.services) {
            /*for (let srv_test_cfg of test_cfg.services) {
              if (srv_cfg.name == srv_test_cfg.name) {
                srv_cfg.config = ObjectUtil.force_add(srv_cfg.config, srv_test_cfg.config);
              }
            }*/

            if (srv_cfg.name == twig_srv) {
              selected_cfg = srv_cfg.config;
              selected_cfg.cwd = test_path;
            }
          }

          LogUtil.msg([1, 45, 37, " > SERVICE >", 0], " > ", twig_srv);
          const tests_dir = path.join(module_dir, cfg.file_names.tests);
          const suite_dir = path.join(tests_dir, cfg.file_names.suite);
          await this.run_test_suite(suite_dir, selected_cfg, zunzun_config, statistics);
        }
      }

      LogUtil.msg(
        [1, 42, 37, `PASSED: ${statistics.passed} / ${statistics.total}`, 0],
        [1, 41, 37, `FAILED: ${statistics.failed} / ${statistics.total}`, 0]
      );
      if (statistics.failed == 0) {
        LogUtil.msg([1, 42, 37, ` ALL TESTS PASSED! `, 0]);
      } else {
        LogUtil.msg([1, 41, 37, ` SOME TESTS FAILED! `, 0]);
      }



      this.tcp_server.close();
      if (!cfg.keep_env && !cfg.no_env) {
        if (this.container) {
          this.container.kill();
        } else {
          this.test_app_process.kill();
          this.test_app_ostream.end();
        }
      }
    } catch (e) {
      console.error(e.stack);
    }
  }


  async run_test_suite(suite_dir, srv_cfg, zunzun_config, statistics) {
    try {
      const _this = this;
      const tests_cfg_path = path.join(suite_dir, this.cfg.file_names.cases);

      if (fs.existsSync(tests_cfg_path)) {
        const test_cases = YAML.parse(fs.readFileSync(tests_cfg_path, "utf8"));

        if (test_cases) {
 
          for (let test_target of test_cases) {


            if (!this.select_cases || this.select_cases.includes(test_target.file)) {

              const target_path = path.resolve(suite_dir, test_target.file);
              const test_module = await FlyModule.load(suite_dir, test_target.file);

              if (
                typeof test_module.default.construct == "function" &&
                test_module.default.construct.constructor.name == 'AsyncFunction'
              ) {statistics
                const test_instance = _this.current_instance = await test_module.default.construct({
                  twig: srv_cfg,
                  env: zunzun_config,
                  zun: this.zuncfg,
                }, this.tools, this.test_app_socket);

                LogUtil.nl();
                LogUtil.msg([1, 46, 37, " > > TEST > >", 0], test_target.name, ">" , test_target.file);
                for (let test_method_obj of test_target.methods) {
                  const start_time = Date.now();
                  const test_method = test_method_obj.name;
                  LogUtil.msg([1, 43, 37, ` > > ( ${statistics.cti} / ${statistics.total} ) > > > >`, 0], [test_method_obj.msg, "; Method:"], [34, test_method, 0]);

                  if (test_instance[test_method].constructor.name == 'AsyncFunction') {
                    try {
                      await test_instance[test_method]();
                      statistics.passed++;
                      const test_duration = TimeUtil.format(Date.now() - start_time, TIME_FORMAT);
                      LogUtil.msg([1, 4, 37, 42, ` TEST METHOD '${test_method}' : "${test_method_obj.msg}"  SUCCEEDED!`, 0], [1, 40, 36, `| TOOK: ${test_duration}`, 0]);
                      LogUtil.nl();
                    } catch (e) {
                      statistics.failed++;
                      LogUtil.msg([1, 4, 37, 41, `TEST METHOD '${test_method}' : "${test_method_obj.msg}"  FAILED!`, 0]);
                      LogUtil.nl();
                      console.log(e.stack);
                    }
                  } else {
                    try {
                      test_instance[test_method]();
                      statistics.passed++;
                      const test_duration = TimeUtil.format(Date.now() - start_time, TIME_FORMAT);
                      LogUtil.msg([1, 4, 37, 42, ` TEST METHOD '${test_method}' : "${test_method_obj.msg}"  SUCCEEDED!`, 0], [1, 40, 36, `| TOOK: ${test_duration}`, 0]);
                      LogUtil.nl();
                    } catch (e) {
                      statistics.failed++;
                      LogUtil.msg([1, 4, 37, 41, `TEST METHOD '${test_method}' : "${test_method_obj.msg}"  FAILED!`, 0]);
                      console.log(e.stack);
                      LogUtil.nl();
                    }
                  }
                  statistics.cti++;
                }


                if (typeof test_instance.destroy == "function") {
                  await test_instance.destroy();
                }
              } else {
                
                LogUtil.nl();
                LogUtil.msg([1, 46, 37, " > > TEST > >", 0], test_target.name, ">" , test_target.file);
                for (let test_method_obj of test_target.methods) {
                  const test_method = test_method_obj.name;
                  LogUtil.msg([1, 43, 37, ` > > ( ${statistics.cti} / ${statistics.total} ) > > > >`, 0], [test_method_obj.msg, "; Method:"], [34, test_method, 0]);

                  if (test_module.default[test_method].constructor.name == 'AsyncFunction') {
                    try {
                      await test_module.default[test_method](srv_cfg, zunzun_config, this.zuncfg);
                      statistics.passed++;
                      LogUtil.msg([1, 4, 37, 42, ` TEST METHOD '${test_method}' : "${test_method_obj.msg}"  SUCCEEDED!`, 0]);
                      LogUtil.nl();
                    } catch (e) {
                      statistics.failed++;
                      LogUtil.msg([1, 4, 37, 41, `TEST METHOD '${test_method}' : "${test_method_obj.msg}"  FAILED!`, 0]);
                      console.log(e.stack);
                      LogUtil.nl();
                    }
                  } else {
                    try {
                      test_module.default[test_method](srv_cfg, zunzun_config, this.zuncfg);
                      statistics.passed++;
                      LogUtil.msg([1, 4, 37, 42, ` TEST METHOD '${test_method}' : "${test_method_obj.msg}"  SUCCEEDED!`, 0]);
                      LogUtil.nl();
                    } catch (e) {
                      statistics.failed++;
                      LogUtil.msg([1, 4, 37, 41, `TEST METHOD '${test_method}' : "${test_method_obj.msg}"  FAILED!`, 0]);
                      console.log(e.stack);
                      LogUtil.nl();
                    }
                  }
                  statistics.cti++;
                }
              }

            }


          }
        }
      } else {
        LogUtil.msg([1, 100, 37, " > NO TESTS <", 0]);
      }
    } catch (e) {
      console.error(e.stack);
    }
  }
}

