
import fs from "fs"
import path from "path"

import ExecUtil from 'zunzun/flyutil/exec.mjs'


export default class Container {

  constructor(cwd, cfg) {
    this.cwd = cwd;
    this.tool = cfg.tool || "docker";
    this.name = cfg.name || "new_container";
    this.network = cfg.network;
    this.persistent = cfg.persistent;
    this.subject_cmd = cfg.subject_cmd;
    this.secondary_subjects = cfg.secondary_subjects;
  }

  reset_volumes(cwd, cfg) {
    const volume_names = [];
    for (let mount in this.persistent) {
      volume_names.push(mount);
    }
    const vol_str = volume_names.join(" ");


    ExecUtil.command(`docker volume rm ${vol_str} || true`);

  }

  spawn() {
    let params = ``;
    
    if (this.network) {
      if (this.network.privileged) params += ` --privileged`;
      if (this.network.bridge) {
        params += ` --network ${this.network.bridge} --add-host=host.internal:172.18.0.1 --dns=172.18.0.1`;
      }
      if (this.network.ip) params += ` --ip ${this.network.ip}`;
      if (this.network.p) {
        for (let ports of this.network.p) {
          params += ` -p ${ports}`;
        }
      }
    }



    for (let mount in this.persistent) {
      if (this.tool == "podman") {
        params += ` -v ${mount}:${this.persistent[mount]}`;
      } else if (this.tool == "docker") {
        params += ` --mount source=${mount},target=${this.persistent[mount]}`;
      }
    }

    const cmd = `${this.tool} run -i --rm --name ${this.name}${params} ${this.name}`;
    const container_process = this.process = ExecUtil.spawn(cmd, this.cwd);

    const log_path = path.join(this.cwd, "test-flight/test-container.log");
    const ostream = this.ostream = fs.createWriteStream(log_path);
    container_process.stdout.pipe(ostream);
    container_process.stderr.pipe(ostream);
  }

  bash(remote_cmd, cwd = undefined, stdio_inherit = undefined) {
    const cmd = `${this.tool} exec ${this.name} bash -c "${remote_cmd}"`;
    return ExecUtil.command(cmd, cwd || this.cwd, typeof stdio_inherit == "undefined" ? true : stdio_inherit);
  }

  kill() {
    const cmd = `${this.tool} stop ${this.name}`;
    ExecUtil.command(cmd, this.cwd, true);
    this.process.kill();
    this.ostream.end();
  }
}
