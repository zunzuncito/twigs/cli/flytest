
import net from 'net'

export default class TCPServer {

  static async construct(cfg, env) {
    const _this = new TCPServer(cfg, env);

    await new Promise((resolve) => {
      function tcp_ready() {
        if (_this.tcp_ready) {
          resolve();
        } else {
          setTimeout(tcp_ready, 333);
        }
      }

      setTimeout(tcp_ready, 333);
    });

    return _this;
  }

  constructor(cfg, env) {
    const _this = this;
    this.test_app_socket = undefined;
    this.secondary_sockets = [];
    this.tcp_server = net.createServer((socket) => {
      console.log('Client connected');
      if (!this.test_app_socket) this.test_app_socket = socket;

      socket.on('data', async (data) => {
        const msgs = data.toString().split('\n').filter((msg) => msg.trim().length > 0).map((msg) => JSON.parse(msg));
        for (let msg of msgs) {
          const msg = JSON.parse(data.toString());
          if (msg.task) {
            if (env.current_instance && typeof env.current_instance.perform_task == "function") {
              const results = await env.current_instance.perform_task(msg.task);
              socket.write(JSON.stringify({
                done_task: msg.task,
                results: results
              })+"\n");
            }
          } else if (msg.target) {
            for (let hook of env.hooks) {
              if (hook.target == msg.target && hook.class_name == msg.class) {
                hook.incoming(msg);
              }
            }
          } else if (msg.subject_name) {
            _this.next_ss = {
              subject_name: msg.subject_name,
              socket
            };
          } else {
            if (env.current_instance && typeof env.current_instance.subject_data == "function") {
              env.current_instance.subject_data(msg);
            }
          }
        }
      });

      socket.on('end', () => {
        console.log('Client disconnected');
      });

      socket.on('error', (err) => {
        console.error('Error:', err.message);
      });
    });

    this.tcp_server.listen(cfg.port, cfg.host, () => {
      console.log(`TCP server running on ${cfg.host}:${cfg.port}`);
      _this.tcp_ready = true;
    });
  }

  async socket() {
    const _this = this;
    return await new Promise((resolve) => {
      function get_socket() {
        if (_this.test_app_socket) {
          resolve(_this.test_app_socket);
        } else {
          setTimeout(get_socket, 333);
        }
      }
      get_socket();
    });
  }

  async next_subject_socket() {
    const _this = this;
    return await new Promise((resolve) => {
      function get_socket() {
        if (_this.next_ss) {
          const nss = _this.next_ss;
          _this.secondary_sockets.push(nss.socket);
          _this.next_ss = undefined;
          resolve(nss);
        } else {
          setTimeout(get_socket, 333);
        }
      }
      get_socket();
    });
  }

  close() {
    this.test_app_socket.destroySoon();
    for (let sec_sock of this.secondary_sockets) {
      sec_sock.destroySoon();
    }
    this.tcp_server.close((err) => {
      if (err) throw err;
    })
  }
}
