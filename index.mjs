
import fs from "fs"
import path from "path"

import YAML from "yaml"

//import CLIUtil from 'zunzun/flyutil/cli.mjs'
import FlyModule from "zunzun/flymodule/index.mjs"

import LogUtil from 'zunzun/flyutil/log.mjs'


import FlyCfg from 'zunzun/flycfg/index.mjs'

import FSUtil from 'zunzun/flyutil/fs.mjs'
import ExecUtil from 'zunzun/flyutil/exec.mjs'

import TestingEnvironment from './env/index.mjs'

const CWD = process.cwd();

const param_regexps = [
  {
    name: "select-services",
    regexp: /^--twigs=.*$/
  },
  {
    name: "select-cases",
    regexp: /^--cases=.*$/
  },
  {
    name: "no-npm",
    regexp: /^--no-npm$/
  },
  {
    name: "no-twigs",
    regexp: /^--no-twigs$/
  },
  {
    name: "no-install",
    regexp: /^--no-install$/
  },
  {
    name: "debug",
    regexp: /^--debug$/
  },
  {
    name: "env-only",
    regexp: /^--env-only$/
  },
  {
    name: "keep-env",
    regexp: /^--keep-env$/
  },
  {
    name: "no-env",
    regexp: /^--no-env$/
  },
  {
    name: "reset-volumes",
    regexp: /^--reset-volumes$/
  }
];


export default class FlyTest {

  static async run(cfg, zuncfg, basecfg) {
    try {

      for (let arg of process.argv) {
        for (let param_regexp of param_regexps) {
          const param_match = arg.match(param_regexp.regexp);
          if (param_match) {
            switch (param_regexp.name) {
              case 'select-services':
                cfg.select_twigs = arg.split("=")[1].split(",");
                break;
              case 'select-cases':
                cfg.select_cases = arg.split("=")[1].split(",");
                break;
              case 'no-npm':
                cfg.no_npm = true;
                break;
              case 'no-twigs':
                cfg.no_twigs = true;
                break;
              case 'no-install':
                cfg.no_npm = true;
                cfg.no_twigs = true;
                break;
              case 'debug':
                cfg.debug = true;
                break;
              case 'env-only':
                cfg.env_only = true;
                break;
              case 'keep-env':
                cfg.keep_env = true;
                break;
              case 'no-env':
                cfg.no_env = true;
                break;
              case 'reset-volumes':
                cfg.reset_volumes = true;
                break;
              default:

            }
          }
        }
      }

      console.debug = cfg.debug ? console.debug : () => {};

      const _this = new FlyTest(cfg, zuncfg, basecfg);
      _this.env = await TestingEnvironment.construct(CWD, cfg, zuncfg, basecfg);
      if (process.argv[3] !== "sync") {
        _this.env.envcfg = await FlyCfg.project(_this.env_path);
        _this.env.envcfg = _this.env.envcfg.app_cfg;
      }


      if (zuncfg) {
        switch (process.argv[3]) {
          case "help":
            console.log(`
Options:
- sync [dev]
- dev
               
            `);
          case "sync":
            _this.env.sync();
            break;
          default:
            _this.env.test();
        }
      } else {
        console.log("NOT ZUN ZUN DIRECTORY");

      }

    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(cfg, zuncfg, basecfg) {

    this.cfg = cfg;
    this.zuncfg = zuncfg;

    this.base_path = path.resolve(CWD, cfg.file_names.tests);
    this.cfg_path = path.resolve(this.base_path, "cfg.yaml");
    this.env_path = path.resolve(this.base_path, "env");
    this.log_path = path.resolve(this.env_path, `zunsrv-test-current.log`);
  }

}
